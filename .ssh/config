# /!\ Ce fichier de configuration nécessite openSSH 7.3+ (Debian Stretch+)
# Pour les version antérieur voir .ssh/config.old
# .ssh/config générique mis à votre disposition
# par les gentils membres actifs du Cr@ns
# Vous pouvez l'utilisez, le redistribuer, le modifier à votre convenance.
# Des questions, des suggestions : {nounou,ca}@lists.crans.org
# Typiquement, vous pouvez proposer d'ajouter la conf pour accéder à votre département
# Licence : WTFPL

# Les sections commentées par #~# sont des features qui ne sont pas activées
# par défaut. Sentez-vous libre de les décommenter pour les utiliser.

# À noter que la plupart des serveurs présents dedans sont des serveurs
# du Cr@ns, qui risquent donc d'intéresser essentiellement des membres actifs.
# Pensez à remplacer "loginCr@ns", "loginFedeRez", "loginBDE" et "loginENS" par
# les logins idoines.

#------------------------------------------------------------------------------

# Les options qui ne sont pas indentées sont activées pour toutes les connexions
# Mêmes celles qui ne correspondent à aucun bloc plus bas
# Activer la compression des données transmises lorsque c'est possible
Compression yes

#~# # Afficher la fingerprint du serveur sous la forme d'un ASCII art
#~# VisualHostKey yes

# Ne pas hasher les noms des machines auxquelles on se connecte dans
# le fichier known_hosts
HashKnownHosts no

# Vérifier la concordance du champ DNS SSHFP de la machine (si existant)
# et du fingerprint présenté par le serveur
VerifyHostKeyDNS yes

# Récupère les nouvelles hostkeys sur le serveur après l'authentification.
# Les options sont yes, no (par défaut), ask.
UpdateHostKeys yes

#~# # Selection l'agent ssh à utiliser. Par défaut, utilise la variable d'environnement
#~# # SSH_AUTH_SOCK
#~# IdentityAgent /path/to/unix/sock

# Ajoute la clef au ssh-agent. Les options possibles sont yes, no, ask.
AddKeysToAgent ask

#~# # Utilise le system de connection Master pour accélérer les connexions multiple
#~# # a un même host.  Attention toutes les connexions sont liés à la première, la
#~# # fermer, fermera toutes les connexions. (voir option suivante)
#~# ControlMaster yes

#~# # Maintient la connexion maitre après la fermeture de la connexion initiale.
#~# # no: la connexion est fermé
#~# # yes: la connexion reste ouverte en arrière plan jusqu'à fermeture explicite
#~# # n: la connexion reste ouverte en arrière plan n secondes si non utilisé
#~# ControlPersist 60

#~# # Certaines QuelqueChoseBox tuent les connexion TCP inactives depuis
#~# # trop longtemps.
#~# # Cette option fait en sorte d'envoyer toutes les 60 secondes un paquet
#~# # sur la connexion, pour la garder vivante.
#~# ServerAliveInterval 60

#~# # Abandonner au bout de 3 échecs (= considérer la connexion comme morte)
#~# ServerAliveCountMax 3

# Les options suivantes apparaissent dans les blocs
# Host = commence un bloc avec les noms qui utiliseront ce bloc
# HostName = nom réellement utilisé pour se connecter au serveur (ou son IP)
# User = nom d'utilisateur distant
# Port = port de connexion (pour override le port 22)
# ForwardAgent = forwarder l'agent ssh sur la machine
#            (il vaut mieux qu'elle et ses administrateurs soient de confiance)
# ProxyJump = pour passer par un autre serveur intermédiaire
#                (pour un serveur qui ne peut pas être contacté directement)

#~# # +-----------------+
#~# # | Serveurs du BDE |
#~# # +-----------------+
#~# # Serveurs du BDE accessibles aux respos info
#~#
#~# Include ~/.ssh/config_bde

#~# # +-------------------+
#~# # | Serveurs du Cr@ns |
#~# # +-------------------+
#~# # Accessible aux apprentis
#~# # sauf zamok et ssh2, accessibles à tous les adhérents
#~#
#~# Include ~/.ssh/config_crans
#~# Include ~/.ssh/config_bornes_crans

#~# # +---------+
#~# # | FedeRez |
#~# # +---------+
#~# # Accessibles aux membres de l'équipe d'admin de FedeRez
#~#
#~# Include ~/.ssh/config_federez


#~# # +-------------------+
#~# # | Serveurs de l'ENS |
#~# # +-------------------+
#~#
#~# Include ~/.ssh/config_ens


#~# # +------------------+
#~# # | Département info |
#~# # +------------------+
#~# # Machines du département informatique de l'ENSC
#~# # Accessibles aux A0
#~#
#~# Include ~/.ssh/config_dptinfo


#~# # +-------------------+
#~# # | Département maths |
#~# # +-------------------+
#~# # Machines du département de mathématiques de l'ENSC
#~# # Accessibles aux A1
#~#
#~# Include ~/.ssh/config_dptmaths

#~# # +-------+
#~# # | Perso |
#~# # +-------+
#~#
#~# Include ~/.ssh/config_perso

# END (utile pour éviter les merge conflicts)
